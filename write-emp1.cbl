       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. KITTIKUN.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO  "emp1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  EMP-FILE.
       01  EMP-DETAILS.
           88 END-OF-EMP-FILE   VALUE  HIGH-VALUE.
           05 EMP-SSN  PIC 9(9).
           05 EMP-NAME.
                 10 EMP-SURNAME PIC X(15).
                 10 EMP-FORENAME PIC X(10). 
           05 EMP-DATE-OF-BIRTH.
                 10 EMP-YOB PIC 9(4).
                 10 EMP-MOB PIC 9(2).
                 10 EMP-DOB PIC 9(2).
           05 EMP-GENDER  PIC X.
           
       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT EMP-FILE 
           MOVE "123456789" TO EMP-SSN 
           MOVE "KITTIKUN" TO EMP-SURNAME
           MOVE "KREDKATOK" TO EMP-FORENAME
           MOVE "20001103" TO EMP-DATE-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS 

           MOVE "987654321" TO EMP-SSN 
           MOVE "PUBADIN" TO EMP-SURNAME 
           MOVE "SUKJAROEN" TO EMP-FORENAME
           MOVE "20010120" TO EMP-DATE-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS 

           MOVE "147258369" TO EMP-SSN 
           MOVE "MANKONG" TO EMP-SURNAME 
           MOVE "LIMPAPONG" TO EMP-FORENAME
           MOVE "20001009" TO EMP-DATE-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS 

           CLOSE EMP-FILE 
           GOBACK 
           .
